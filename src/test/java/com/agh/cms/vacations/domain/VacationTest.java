package com.agh.cms.vacations.domain;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Period;

import static org.junit.Assert.*;

public class VacationTest {

    private final static int CURRENT_YEAR = LocalDate.now().getYear();

    @Test
    public void vacationLengthInCurrentYear_whenVacationWasInLastYear_returns0() {
        Vacation vacation = new Vacation(null, LocalDate.of(CURRENT_YEAR - 1, 5, 5), LocalDate.of(CURRENT_YEAR - 1, 5, 10));

        int length = vacation.vacationLengthInCurrentYear();

        assertEquals(0, length);
    }

    @Test
    public void vacationLengthInCurrentYear_whenVacationWillBeInNextYear_returns0() {
        Vacation vacation = new Vacation(null, LocalDate.of(CURRENT_YEAR + 1, 5, 5), LocalDate.of(CURRENT_YEAR + 1, 5, 10));

        int length = vacation.vacationLengthInCurrentYear();

        assertEquals(0, length);
    }

    @Test
    public void vacationLengthInCurrentYear_whenVacationStartedInLastYear_returnsPeriodBetweenBeginningOfTheYearAndEndDate() {
        LocalDate endDate = LocalDate.of(CURRENT_YEAR, 1, 5);
        LocalDate beginOfTheYear = LocalDate.of(CURRENT_YEAR, 1, 1);
        Vacation vacation = new Vacation(null, LocalDate.of(CURRENT_YEAR - 1, 12, 28), endDate);

        int length = vacation.vacationLengthInCurrentYear();

        assertEquals(Period.between(beginOfTheYear, endDate).getDays(), length);
    }

    @Test
    public void vacationLengthInCurrentYear_whenVacationWillEndInNextYear_returnsPeriodBetweenStartDateAndEndOfTheYear() {
        LocalDate startDate = LocalDate.of(CURRENT_YEAR, 12, 25);
        LocalDate endOfTheYear = LocalDate.of(CURRENT_YEAR, 12, 31);
        Vacation vacation = new Vacation(null, startDate, LocalDate.of(CURRENT_YEAR + 1, 1, 5));

        int length = vacation.vacationLengthInCurrentYear();

        assertEquals(Period.between(startDate, endOfTheYear).getDays(), length);
    }

    @Test
    public void vacationLengthInCurrentYear_whenVacationIsInCurrentYear_returnsPeriodBetweenStartDateAndEndDate() {
        LocalDate startDate = LocalDate.of(CURRENT_YEAR, 5, 5);
        LocalDate endDate = LocalDate.of(CURRENT_YEAR, 5, 15);
        Vacation vacation = new Vacation(null, startDate, endDate);

        int length = vacation.vacationLengthInCurrentYear();

        assertEquals(Period.between(startDate, endDate).getDays(), length);
    }
}