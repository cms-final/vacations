package com.agh.cms.vacations.context.dto.validation;

import com.agh.cms.vacations.context.dto.VacationAddRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public final class VacationAddRequestValidator implements ConstraintValidator<VacationAddRequestValid, VacationAddRequest> {

    @Override
    public void initialize(VacationAddRequestValid constraintAnnotation) {
    }

    @Override
    public boolean isValid(VacationAddRequest request, ConstraintValidatorContext context) {
        return request.endDate.isAfter(request.startDate);
    }
}
