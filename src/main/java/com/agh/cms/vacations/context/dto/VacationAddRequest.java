package com.agh.cms.vacations.context.dto;

import com.agh.cms.vacations.context.dto.validation.VacationAddRequestValid;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Validated
@VacationAddRequestValid
public class VacationAddRequest {

    @FutureOrPresent(message = "start date can't be from past")
    @NotNull(message = "start date can't be null")
    public LocalDate startDate;

    @FutureOrPresent(message = "end date can't be from past")
    @NotNull(message = "end date can't be null")
    public LocalDate endDate;
}
