package com.agh.cms.vacations.context.dto;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Validated
public final class UserRegisterRequest {

    @NotNull(message = "days off quantity can't be null")
    public Integer daysOffQuantity;
}
