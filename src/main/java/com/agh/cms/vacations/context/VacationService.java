package com.agh.cms.vacations.context;

import com.agh.cms.common.domain.UserBasicInfo;
import com.agh.cms.common.domain.dto.ReportCreateRequest;
import com.agh.cms.common.domain.dto.ReportType;
import com.agh.cms.common.domain.exception.BusinessException;
import com.agh.cms.common.domain.exception.ResourceNotFoundException;
import com.agh.cms.vacations.context.dto.UserRegisterRequest;
import com.agh.cms.vacations.context.dto.VacationAddRequest;
import com.agh.cms.vacations.context.dto.VacationBasicInfo;
import com.agh.cms.vacations.domain.*;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

@Service
class VacationService {

    private final UserRepository userRepository;
    private final VacationRepository vacationRepository;
    private final EventRepository eventRepository;
    private final UserBasicInfo userBasicInfo;
    private final ReportRepository reportRepository;

    VacationService(UserRepository userRepository, VacationRepository vacationRepository, EventRepository eventRepository,
                    UserBasicInfo userBasicInfo, ReportRepository reportRepository) {
        this.userRepository = userRepository;
        this.vacationRepository = vacationRepository;
        this.eventRepository = eventRepository;
        this.userBasicInfo = userBasicInfo;
        this.reportRepository = reportRepository;
    }

    Integer userDaysOffLeft() {
        UserInfo userInfo = userRepository.findById(userBasicInfo.username())
                .orElseThrow(() -> new ResourceNotFoundException(userBasicInfo.username() + " not found"));
        return userInfo.daysOffLeft();
    }

    void registerUser(UserRegisterRequest request) {
        UserInfo userInfo = new UserInfo(userBasicInfo.username(), request.daysOffQuantity);
        userRepository.save(userInfo);
    }

    VacationBasicInfo addVacation(VacationAddRequest request) {
        UserInfo userInfo = userRepository.findById(userBasicInfo.username())
                .orElseThrow(() -> new ResourceNotFoundException(userBasicInfo.username() + " not found"));
        int vacationLength = Period.between(request.startDate, request.endDate).getDays();
        Integer daysOffLeft = userInfo.daysOffLeft();
        if (vacationLength > daysOffLeft) {
            throw new BusinessException("You don't have enough days off left");
        }
        Vacation vacation = new Vacation(userInfo, request.startDate, request.endDate);
        eventRepository.createEvent(vacation.event());
        vacationRepository.save(vacation);
        return vacation.basicInfo();
    }

    byte[] createReport() {
        if (!userRepository.existsById(userBasicInfo.username())) {
            throw new ResourceNotFoundException(userBasicInfo.username() + " not found");
        }
        ReportCreateRequest request = new ReportCreateRequest();
        request.worker = userBasicInfo.username();
        request.reportType = ReportType.VACATIONS;
        List<Vacation> vacations = vacationRepository.findByUserInfo_Username(userBasicInfo.username());
        request.reportRows = vacations.stream()
                .map(Vacation::reportRow)
                .collect(Collectors.toList());
        return reportRepository.createReport(request);
    }
}
