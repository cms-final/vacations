package com.agh.cms.vacations.context.dto;

import java.time.LocalDate;

public final class VacationBasicInfo {

    public String username;
    public LocalDate startDate;
    public LocalDate endDate;
}
