package com.agh.cms.vacations.context;

import com.agh.cms.vacations.context.dto.UserRegisterRequest;
import com.agh.cms.vacations.context.dto.VacationAddRequest;
import com.agh.cms.vacations.context.dto.VacationBasicInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/vacations")
class VacationEndpoint {

    private final VacationService service;

    VacationEndpoint(VacationService service) {
        this.service = service;
    }

    @PostMapping("/userRegistration")
    HttpStatus registerUser(@Valid @RequestBody UserRegisterRequest request) {
        service.registerUser(request);
        return HttpStatus.OK;
    }

    @GetMapping("/daysOffLeft")
    ResponseEntity<Integer> userDaysOffLeft() {
        return ResponseEntity.ok(service.userDaysOffLeft());
    }

    @PostMapping
    ResponseEntity<VacationBasicInfo> addVacation(@Valid @RequestBody VacationAddRequest request) {
        VacationBasicInfo vacationBasicInfo = service.addVacation(request);
        return ResponseEntity.ok(vacationBasicInfo);
    }

    @GetMapping("/report")
    ResponseEntity<byte[]> createReport() {
        byte[] report = service.createReport();
        return ResponseEntity.ok(report);
    }
}
