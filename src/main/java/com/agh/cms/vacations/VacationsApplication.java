package com.agh.cms.vacations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.agh.cms")
@EntityScan(basePackages = "com.agh.cms")
@EnableFeignClients
@EnableDiscoveryClient
public class VacationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(VacationsApplication.class, args);
    }
}

