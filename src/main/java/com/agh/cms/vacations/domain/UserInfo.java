package com.agh.cms.vacations.domain;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Access(AccessType.FIELD)
@Table(schema = "vacations", name = "user_info")
public class UserInfo {

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "days_off_quantity")
    private Integer daysOffQuantity;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "userInfo")
    private Set<Vacation> vacations;

    private UserInfo() {
    }

    public UserInfo(String username, Integer daysOffQuantity) {
        this.username = username;
        this.daysOffQuantity = daysOffQuantity;
    }

    public String username() {
        return username;
    }

    public Integer daysOffLeft() {
        return daysOffQuantity - vacations.stream()
                .map(Vacation::vacationLengthInCurrentYear)
                .reduce((a, b) -> a + b)
                .orElse(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfo userInfo = (UserInfo) o;
        return Objects.equals(username, userInfo.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }

    @Override
    public String toString() {
        return "UserInfo{" + "username='" + username + '\'' + '}';
    }
}
