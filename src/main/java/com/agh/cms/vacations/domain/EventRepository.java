package com.agh.cms.vacations.domain;

import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;

public interface EventRepository {

    EventBasicInfo createEvent(EventCreateRequest newEvent);
}
