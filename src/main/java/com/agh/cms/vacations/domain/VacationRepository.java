package com.agh.cms.vacations.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VacationRepository extends JpaRepository<Vacation, Integer> {

    List<Vacation> findByUserInfo_Username(String username);
}
