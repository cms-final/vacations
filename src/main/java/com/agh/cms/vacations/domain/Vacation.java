package com.agh.cms.vacations.domain;

import com.agh.cms.common.domain.EventType;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.common.domain.dto.ReportRow;
import com.agh.cms.vacations.context.dto.VacationBasicInfo;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.Objects;

@Entity
@Access(AccessType.FIELD)
@Table(schema = "vacations", name = "vacation")
public class Vacation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "username")
    private UserInfo userInfo;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    private Vacation() {
    }

    public Vacation(UserInfo userInfo, LocalDate startDate, LocalDate endDate) {
        this.userInfo = userInfo;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public VacationBasicInfo basicInfo() {
        VacationBasicInfo vacationBasicInfo = new VacationBasicInfo();
        vacationBasicInfo.username = userInfo.username();
        vacationBasicInfo.startDate = startDate;
        vacationBasicInfo.endDate = endDate;
        return vacationBasicInfo;
    }

    public EventCreateRequest event(){
        EventCreateRequest eventCreateRequest = new EventCreateRequest();
        eventCreateRequest.title = "Vacation";
        eventCreateRequest.type = EventType.VACATION;
        eventCreateRequest.startDate = startDate.atStartOfDay();
        eventCreateRequest.endDate = endDate.atTime(23, 59);
        eventCreateRequest.users = Collections.singleton(userInfo.username());
        eventCreateRequest.groups = Collections.emptySet();
        return eventCreateRequest;
    }

    public ReportRow reportRow() {
        ReportRow reportRow = new ReportRow();
        reportRow.from = startDate.toString();
        reportRow.to = endDate.toString();
        return reportRow;
    }

    public int vacationLengthInCurrentYear() {
        int currentYear = LocalDate.now().getYear();
        if (startDate.getYear() == currentYear && endDate.getYear() == currentYear) {
            return Period.between(startDate, endDate).getDays();
        }
        if (startDate.getYear() == currentYear - 1 && endDate.getYear() == currentYear) {
            return Period.between(LocalDate.of(currentYear, 1, 1), endDate).getDays();
        }
        if (startDate.getYear() == currentYear && endDate.getYear() == currentYear + 1) {
            return Period.between(startDate, LocalDate.of(currentYear, 12, 31)).getDays();
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vacation vacation = (Vacation) o;
        return Objects.equals(id, vacation.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Vacation{" + "id=" + id + '}';
    }
}
