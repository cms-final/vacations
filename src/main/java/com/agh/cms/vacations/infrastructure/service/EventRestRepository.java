package com.agh.cms.vacations.infrastructure.service;

import com.agh.cms.common.domain.dto.EventBasicInfo;
import com.agh.cms.common.domain.dto.EventCreateRequest;
import com.agh.cms.vacations.domain.EventRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
class EventRestRepository implements EventRepository {

    private final EventsClient eventsClient;

    EventRestRepository(EventsClient eventsClient) {
        this.eventsClient = eventsClient;
    }

    @Override
    public EventBasicInfo createEvent(EventCreateRequest newEvent) {
        ResponseEntity<EventBasicInfo> event = eventsClient.createEvent(newEvent);
        return event.getBody();
    }
}
